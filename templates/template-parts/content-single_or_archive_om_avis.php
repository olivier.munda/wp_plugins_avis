<?php

/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Le_thème_de_OM
 */
?>

<article>
    <header class="entry-header">
        <?php
        if (is_singular()) :
            the_title('<h1 class="entry-title">', '</h1>');
        else :
            the_title('<h2 class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h2>');
        endif;

        if ('om_avis' === get_post_type()) :
        ?>
            <div class="entry-meta">
                <?php
                tcoif_posted_on();
                ?>
            </div><!-- .entry-meta -->
        <?php endif; ?>
    </header><!-- .entry-header -->

    <?php tcoif_post_thumbnail(); ?>

    <div class="entry-content">
        <?php the_content(); ?>
        <p>Reviewer: <?php the_field('om_avis_reference'); ?> <?php the_field('om_avis_nom'); ?></p>
        <p>Editeur: <?php the_field('om_avis_prenom'); ?></p>
        <p>Review: <?php the_field('om_avis_review'); ?></p>

        <?php
        wp_link_pages(array(
            'before' => '<div class="page-links">' . esc_html__('Pages:', 'tcoif'),
            'after'  => '</div>',
        ));
        ?>
    </div><!-- .entry-content -->

    <footer class="entry-footer">
        <?php tcoif_entry_footer(); ?>
    </footer><!-- .entry-footer -->
</article>