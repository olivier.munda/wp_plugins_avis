<?php

/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Le_thème_de_OM
 */
?>

<article>
    <header class="entry-header">
        <?php the_title('<h1 class="entry-title">', '</h1>'); ?>
    </header><!-- .entry-header -->

    <?php tcoif_post_thumbnail(); ?>

    <div class="entry-content">
        <?php
        the_content();

        acf_form('new-avis');
        ?>
    </div><!-- .entry-content -->
</article>