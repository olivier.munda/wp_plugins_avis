<?php

/**
 * Plugin Name: avis
 * Plugin URI: http://localhost/wordpress/wp-content/plugins/avis
 * Description: affiche des avis certifiés
 * Version: 20200420
 * Author: Olivier Munda
 * Author URI: https://omunda@labo-ve.fr/
 * License: GPLv2
 */


// // Define path and URL to the current plugin.
// define('OM_AVIS_PATH', plugin_dir_path(__FILE__));

// // Define path and URL to the ACF plugin.
// define('OM_ACF_PATH', OM_AVIS_PATH . '/includes/acf/');
// define('OM_ACF_URL', plugin_dir_url(__FILE__) . '/includes/acf/');

// // Include the ACF plugin.
// include_once(OM_ACF_PATH . 'acf.php');

// // Customize the url setting to fix incorrect asset URLs.

// // function om_acf_settings_url($url)
// // {
// //     return OM_ACF_URL;
// // };
// add_filter('acf/settings/url');

// (Optional) Hide the ACF admin menu item.
// function om_acf_settings_show_admin($show_admin)
// {
// 	return false;
// }
// add_filter('acf/settings/show_admin', 'om_acf_settings_show_admin');

if (!function_exists('om_avis')) {

    // Register Custom Post Type
    function om_avis()
    {

        $labels = array(
            'name'                  => _x('Avis', 'Post Type General Name', 'om_avis_text'),
            'singular_name'         => _x('Avis', 'Post Type Singular Name', 'om_avis_text'),
            'menu_name'             => __('Avis', 'om_avis_text'),
            'name_admin_bar'        => __('Avis', 'om_avis_text'),
            'archives'              => __('Avis_Archives', 'om_avis_text'),
            'attributes'            => __('Avis_Attributes', 'om_avis_text'),
            'parent_item_colon'     => __('Parent Avis:', 'om_avis_text'),
            'all_items'             => __('All Avis', 'om_avis_text'),
            'add_new_item'          => __('Add New Avis', 'om_avis_text'),
            'add_new'               => __('Add Avis', 'om_avis_text'),
            'new_item'              => __('New Avis', 'om_avis_text'),
            'edit_item'             => __('Edit Avis', 'om_avis_text'),
            'update_item'           => __('Update Avis', 'om_avis_text'),
            'view_item'             => __('View Avis', 'om_avis_text'),
            'view_items'            => __('View Avis', 'om_avis_text'),
            'search_items'          => __('Search Avis', 'om_avis_text'),
            'not_found'             => __('Not found', 'om_avis_text'),
            'not_found_in_trash'    => __('Not found in Trash', 'om_avis_text'),
            'featured_image'        => __('Featured Image', 'om_avis_text'),
            'set_featured_image'    => __('Set featured image', 'om_avis_text'),
            'remove_featured_image' => __('Remove featured image', 'om_avis_text'),
            'use_featured_image'    => __('Use as featured image', 'om_avis_text'),
            'insert_into_item'      => __('Insert into Avis', 'om_avis_text'),
            'uploaded_to_this_item' => __('Uploaded to this Avis', 'om_avis_text'),
            'items_list'            => __('Avis list', 'om_avis_text'),
            'items_list_navigation' => __('Avis list navigation', 'om_avis_text'),
            'filter_items_list'     => __('Filter Avis list', 'om_avis_text'),
        );
        $rewrite = array(
            'slug'                  => 'Avis',
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => true,
        );
        $args = array(
            'label'                 => __('Avis', 'om_avis_text'),
            'description'           => __('Avis clients', 'om_avis_text'),
            'labels'                => $labels,
            'supports'              => array('title', 'editor', 'thumbnail'),
            'taxonomies'            => array('category'),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 20,
            'menu_icon'             => 'dashicons-thumbs-up',
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => true,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'post',
        );
        register_post_type('om_avis', $args);
    }
    add_action('init', 'om_avis', 0);
};


if (function_exists('acf_add_local_field_group')) :

    acf_add_local_field_group(array(
        'key' => 'group_5e9d5d1f30094',
        'title' => 'Avis',
        'fields' => array(
            array(
                'key' => 'field_5e9d5d2f56b1c',
                'label' => 'Référence',
                'name' => 'om_avis_reference',
                'type' => 'text',
                'instructions' => 'renseigner une référence d\'achat ou un numéro de facture.',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => 'FA-123456',
                'prepend' => '',
                'append' => '',
                'maxlength' => 15,
            ),
            array(
                'key' => 'field_5e9d6009716d3',
                'label' => 'Nom',
                'name' => 'om_avis_nom',
                'type' => 'text',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => 15,
            ),
            array(
                'key' => 'field_5e9d60499965b',
                'label' => 'Prénom',
                'name' => 'om_avis_prenom',
                'type' => 'text',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => 15,
            ),
            array(
                'key' => 'field_5e9d60720a63b',
                'label' => 'Avis',
                'name' => 'om_avis_avis',
                'type' => 'textarea',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => '',
                'new_lines' => 'wpautop',
            ),
            array(
                'key' => 'field_5e9d61460a63c',
                'label' => 'Photo',
                'name' => 'om_avis_photo',
                'type' => 'image',
                'instructions' => 'Mini: 50 x 50 px
Maxi: 250 x 250 px
Only .GIF .JPG and .JPEG',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'array',
                'preview_size' => 'thumbnail',
                'library' => 'all',
                'min_width' => 50,
                'min_height' => 50,
                'min_size' => '',
                'max_width' => 250,
                'max_height' => 250,
                'max_size' => 4,
                'mime_types' => '',
            ),
            array(
                'key' => 'field_5e9d626f78ace',
                'label' => 'Note',
                'name' => 'om_avis_note',
                'type' => 'range',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => 3,
                'min' => 1,
                'max' => 5,
                'step' => '',
                'prepend' => '',
                'append' => '',
            ),
            array(
                'key' => 'field_5e9d62b815f00',
                'label' => 'Date',
                'name' => 'om_avis_date',
                'type' => 'date_picker',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'display_format' => 'd/m/Y',
                'return_format' => 'd/m/Y',
                'first_day' => 1,
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'om_avis',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => array(
            0 => 'permalink',
            1 => 'the_content',
            2 => 'excerpt',
            3 => 'discussion',
            4 => 'comments',
            5 => 'slug',
            6 => 'author',
            7 => 'format',
            8 => 'featured_image',
            9 => 'categories',
            10 => 'tags',
        ),
        'active' => true,
        'description' => '',
    ));

endif;

// Register new-book ACF front form
function om_avis_acf_form_init()
{

    // Check function exists.
    if (function_exists('acf_register_form')) {

        // Register form.
        acf_register_form(array(
            'id'                    => 'new-avis',
            'post_id'               => 'new_post',
            'new_post'              => array(
                'post_type'   => 'om_avis',
                'post_status' => 'publish',
            ),
            'field_groups'          => array(
                'group_5e9d5d1f30094'
            ),
            'submit_value'          => __('Confirm', 'om_avis'),
            'updated_message'       => __('Review updated', 'om_avis'),
            'label_placement'       => 'left',
            'instruction_placement' => 'field',
        ));
    }
}

add_action('acf/init', 'om_avis_acf_form_init');

function om_avis_add_single_template($om_template)
{
    if (!$om_template) {
        $om_template = OM_AVIS_PATH . 'templates/single-om_avis.php';
    }
    return $template;
}

add_filter('single-om_avis_template', 'om_avis_add_single_template');

function om_avis_add_archive_template($om_template)
{
    if (!$om_template) {
        $om_template = OM_AVIS_PATH . 'templates/archive-om_avis.php';
    }
    return $template;
}

add_filter('archive-om_avis_template', 'om_avis_add_archive_template');
